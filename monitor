#!/usr/bin/env node

var blessed = require('blessed'),
    contrib = require('blessed-contrib'),
    _ = require('lodash'),
    AWS = require('aws-sdk'),
    moment = require('moment'),
    colors = require('colors/safe'),
    view = require('./lib/view');

var options = require('yargs')
    .usage("Usage: $0 <stackName> <refreshRate>")
    .version('1.0.0', 'version').alias('version', 'V')
    .command("stackName", "Name of the stack to watch", {
        alias: "stack"
    })
    .command("region", "In which region the stack is located", {
        alias: "region"
    })
     .default('region','ap-southeast-2')
     .command("refreshRate", "How often to refresh the displayed data", {
        alias: "rate"
    })
     .default('refreshRate',1)
    .required(1, "You must provide a stack name")
    .help("?")
    .alias("?", "help")
    .example("$0 productionStack us-east-1 10", "Monitor the production stack in us-east-1 updating every 10 seconds")
    .epilog("For bugs and help see https://bitbucket.org/mteichtahl-admin/nodecloudformationmonitor\nMarc Teichtahl <teichtah@amazon.com>")
    .argv;

var stackName = options._[0],
    rawTemplate,
    updateRate = (options._[2] ?options._[2] : 1)*1000,
    stackAge = 0,
    stackCreationTime = 0,
    grid = undefined,
    CloudFormation = new AWS.CloudFormation({
        region: options._[1] ? options._[1]: 'ap-southeast-2'
    }),
    params = {
        NextToken: 'STRING_VALUE',
        StackName: stackName
    },
    resourceParams = {
        StackName: stackName
    };

view.init();
view.title("Cloudformation Monitor");
grid = view.grid({});

var infoForm = grid.set(0, 0, 4, 6, blessed.form, {
    parent: view.screen(),
    width: '100%',
    label: 'Stack Information',
    border: {
        type: 'line'
    }
});

var progressGauge = grid.set(2, 0, 2, 2, contrib.gauge, {
    label: 'Progress',
    stroke: 'green',
    fill: 'white',
});

var resourceTypes = grid.set(2, 2, 2, 4, contrib.bar, {
    label: 'Main Resource Count',
    barWidth: 4,
    barSpacing: 6,
    xOffset: 0,
    maxHeight: 9
});

var log = grid.set(4, 0, 8, 6, contrib.log, {
        label: 'Cloudformation Details',
        fg: "white",
        label: 'Stack Events',
        scrollable: true,
        scrollOnInput: true,
        interactive: true
    }),
    resourceTable = grid.set(0, 6, 12, 6, contrib.table, {
        keys: true,
        interactive: true,
        fg: 'white',
        selectedFg: 'white',
        selectedBg: 'blue',
        interactive: false,
        label: 'Resources & Events',
        border: {
            type: "line"
        },
        columnSpacing: 9,
        columnWidth: [20, 20, 18, 10]
    }),
    StackName = blessed.input({
        parent: infoForm,
        name: 'input',
        input: true,
        keys: true,
        top: 0,
        left: 0,
        height: 1,
        width: '50%',
        style: {
            fg: 'white',
            bg: 'black',
        }
    }),
    StackDescription = blessed.input({
        parent: infoForm,
        name: 'input',
        input: true,
        keys: true,
        top: 1,
        left: 0,
        height: 1,
        width: '90%',
    }),
    StackCreated = blessed.input({
        parent: infoForm,
        name: 'input',
        input: true,
        keys: true,
        top: 2,
        left: 0,
        height: 1,
        width: '90%',
    }),
    StackStatus = blessed.input({
        parent: infoForm,
        name: 'input',
        input: true,
        keys: true,
        top: 3,
        left: 0,
        height: 1,
        width: '90%'
    }),
    StackResourceCount = blessed.input({
        parent: infoForm,
        name: 'input',
        input: true,
        keys: true,
        top: 4,
        left: 0,
        height: 1,
        width: '90%'
    });



view.screen().render();

CloudFormation.describeStacks(params, function(err, data) {
    var self = this;
    if (data) {
        stackCreationTime = data.Stacks[0].CreationTime;
        stackAge = moment().unix() - stackCreationTime;
        if (data) {
            StackName.setLabel("Stack Name:\t " + data.Stacks[0].StackName);
            StackDescription.setLabel("Description:\t" + data.Stacks[0].Description);
            StackCreated.setLabel("Created:\t\t" + data.Stacks[0].CreationTime + " " + moment(stackCreationTime).fromNow());
            StackStatus.setLabel("Status:\t\t " + view.statusColor(data.Stacks[0].StackStatus));
            StackResourceCount.setLabel("Resources:\t  0");
        }
    }
});

setInterval(function(args) {
    var self = this;

    CloudFormation.describeStackEvents(params, function(err, data) {
        if (err) {
            log.log(moment().format('H:mm:ss.SSS') + ' ' + err.stack); // an error occurred
        } else {
            stackAge = moment().unix() - stackCreationTime;
            _.each(data.StackEvents.reverse(), function(event, index) {
                    if (event.NextToken) {
                        params.NextToken = NextToken;
                    }

                    log.log(' ' + moment(event.Timestamp).format('H:mm:ss') + ' ' + event.ResourceType.substr(5) + ' (' + event.LogicalResourceId + ') ' + view.statusColor(event.ResourceStatus));

                }) // successful response
        }
    });

    CloudFormation.describeStacks(params, function(err, data) {
        if (data) {
            var update = data.Stacks[0].StackStatusReason,
                updateText = update ? ' - ' + update : '';

            StackStatus.setLabel("Status:\t\t " + view.statusColor(data.Stacks[0].StackStatus) + updateText);
            StackCreated.setLabel("Created:\t\t" + data.Stacks[0].CreationTime + " " + moment(stackCreationTime).fromNow());
        }
    });

    CloudFormation.describeStackResources(resourceParams, function(err, data) {

        if (data) {

            var stackResources = data['StackResources'],
                completeCount = _.where(stackResources, {
                    'ResourceStatus': 'CREATE_COMPLETE'
                }),
                inProgressCount = _.where(stackResources, {
                    'ResourceStatus': 'CREATE_IN_PROGRESS'
                }),
                createFailedCount = _.where(stackResources, {
                    'ResourceStatus': 'CREATE_FAILED'
                }),
                EC2Instances = _.where(stackResources, {
                    'ResourceType': 'AWS::EC2::Instance'
                }),
                Vpcs = _.where(stackResources, {
                    'ResourceType': 'AWS::EC2::VPC'
                }),
                subnets = _.where(stackResources, {
                    'ResourceType': 'AWS::EC2::Subnet'
                }),
                securityGroups = _.where(stackResources, {
                    'ResourceType': 'AWS::EC2::SecurityGroup'
                }),
                internetGateways = _.where(stackResources, {
                    'ResourceType': 'AWS::EC2::InternetGateway'
                }),
                vpnGateways = _.where(stackResources, {
                    'ResourceType': 'AWS::EC2::VPNGateway'
                }),
                routes = _.where(stackResources, {
                    'ResourceType': 'AWS::EC2::Route'
                }),
                loadBalancers = _.where(stackResources, {
                    'ResourceType': "AWS::ElasticLoadBalancing::LoadBalancer"
                }),
                stacks = _.where(stackResources, {
                    'ResourceType': "AWS::CloudFormation::Stack"
                }),
                EIPs = _.where(stackResources, {
                    'ResourceType': "AWS::EC2::EIP"
                }),
                gateways = vpnGateways.length + internetGateways.length,
                progressPercent = Math.ceil((completeCount.length / stackResources.length) * 100),
                inProgressPercent = Math.floor((inProgressCount.length / stackResources.length) * 100);

            if (inProgressPercent > 0) {
                progressGauge.setStack([{
                    percent: progressPercent,
                    stroke: 'green'
                }, {
                    percent: inProgressPercent,
                    stroke: 'yellow'
                }]);
            } else {
                progressGauge.setStack([{
                    percent: progressPercent,
                    stroke: 'green'
                }]);
            }

            resourceTypes.setData({
                titles: ['Stks', 'VPC', 'Inst', 'EIP', 'Rts', "Subn", "SG", "ELB", "GW"],
                data: [
                    stacks.length + 1,
                    Vpcs.length,
                    EC2Instances.length,
                    EIPs.length,
                    routes.length,
                    subnets.length,
                    securityGroups.length,
                    loadBalancers.length,
                    gateways
                ]
            });


            StackResourceCount.setLabel("Resources:\t  " +
                stackResources.length +
                " (" + colors.green(completeCount.length) +
                '/' + colors.yellow(inProgressCount.length) +
                '/' + colors.red(createFailedCount.length) + ')');


            var tableData = [];

            _.each(stackResources, function(data, index) {
                var then = moment(data.Timestamp).unix(),
                    now = moment().unix(),
                    diff = (now - then),
                    age = diff / 60 < 1 ? diff + ' s' : (diff / 60).toFixed(1) + ' min',
                    status = data.ResourceStatus.substring(0, 18);

                tableData.push([
                    data.LogicalResourceId.substring(0, 20),
                    data.PhysicalResourceId ? data.PhysicalResourceId.substring(0, 20) : data.LogicalResourceId.substring(0, 20),
                    view.statusColor(status),
                    age
                ])
            });

            resourceTable.setData({
                headers: ['Type', 'Id', 'Status', 'Age'],
                data: tableData
            });
        }
    });


}, updateRate)
