# Things to do


| Issue | Description|
|-------|------------|
| Make Resources & Events list scrollable | Stacks often have many many resources and this list can be longer than the current space allowed|
| Make stack events box one line longer | Stack events box is one line too short and needs to be aligned to the bottom of the resources and events box |
| Not sub-stack aware | Should be able to display resoruces etc in sub-stacks - or at least make sub-stacks selectable |
| Regions defaults dont work when different to aws cli  profile | When the default region in the users aws cli profile, there is a conflict and the defaults are not respected |
