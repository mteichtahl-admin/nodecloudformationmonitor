# Cloudformation Monitor
---

**THIS IS A MINIMUM VIABLE PRODUCT RELEASE - I NEED YOUR INPUT TO MAKE THIS BETTER**

## What is this thing ?

Cloudformation Monitors provides the user a textual, yet visual representation of the progress, status, resources and logging of an AWS Cloudformation template throughout the life cycle of the Cloudformation stack.

## Who is tool for

This tool is for any one who uses Cloudformation. This tools makes keeping an eye on a Cloudformation stack quick, easy and configurable.


## What does it look like ?

![](https://teichtah-files.s3.amazonaws.com/public/nodeCFN.png)

## Pre-requisites

- node.js version >= 5.3.0
- npm version >= 3.3.12
- aws-cli >=1.9.15 (Python/2.7.10 Darwin/15.3.0 botocore/1.3.15)

## Installing code.amazon.com

- Clone the repository  `git clone ssh://git.amazon.com/pkg/NodeCloudFormationMonitor`
	- This will create a directory called NodeCloudFormationMonitor. Change into this directory
- Install the required NPM packages `npm install`

## Installing from Bitbucket

- Clone the repository  `git clone https://mteichtahl-admin@bitbucket.org/mteichtahl-admin/nodecloudformationmonitor.git `
- Install the NPM package `npm install`

If you run windows your on your own - as I haven't tested it on that OS yet.


## How do you run this tool

Running the tool is very easy.

```monitor YOUR_STACK_NAME```

where, `YOUR_STACK_NAME` is the name of a Cloudformation stack.

There are also a number of options you can provide.

```
Commands:
  stackName    Name of the stack to watch
  region       In which region the stack is located
  refreshRate  How often to refresh the displayed data

Options:
  --version, -V  Show version number                                   [boolean]
  -?, --help     Show help                                             [boolean]
  --region                                           [default: "ap-southeast-2"]
  --refreshRate                                                     [default: 1]
  ```

## What each section means

The main screen is laid out to provide screen space for 5 key information "blocks".

### Stack Information

This section show simple generic information about your stack.

The resources section shows four numbers. Below is an explanation of what these mean

` 12 (7/3/2) `

Where;
- 12 represents that total number of resources in the stack at the stacks current stage of its life cycle.
- 7 (shown in green) represents the number of successfully created resources
- 3 (shown in orange) represents the number of resources in their "CREATE_IN_PROGRESS" state.
- 2 (shown in red) represents the number of resources in their "CREATE_FAILED" state.

### Progress

This is a simple progress bar representing the number of resources successfully created as a percentage of the total number of resources in the stack at that time.

**Note** Due to the dependency nature of Cloudformation, and the likelihood that that these dependencies will create resources **not** specifically described in the Cloudformation template, this percentage is progress at a point in time. It is possible that this percentage changes over time as dependencies are created. However, in all cases, at the successful completion of the stack creation (STACK_COMPLETE) this will always read 100%.

### Main Resource Count

The Main Resource Count is a bar chart of the number of resources created for the following resources.

- **Stks** Total number of stacks
- **VPC** Total number of Virtual Private Clouds (VPC)
- **Inst** Number of instances created
- **EIP** Number of Elastic IP address
- **Rts** Number of routes created across all route tables
- **Subn** Total number of subnets
- **SG** Total number of Security Groups
- **ELB** Number of Elastic Load Balancers
- **GW** Total number of gateways (CGW, IGW, VGW)

### Stack Events

A list of Cloudformation events that is refreshed every "refreshRate" seconds with a default of 1 sec.

### Resources & Events

A table describing the following

- **Type** - The type of the resource being created
- **Id** - The unique identifier of the resource being created
- **Status** - Status of the resource being created
- **Age** - How long the resource has been in its current state.
