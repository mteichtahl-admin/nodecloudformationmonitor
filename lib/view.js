var blessed = require('blessed'),
    contrib = require('blessed-contrib'),
    colors = require('colors/safe'),
    _ = require('lodash');

var mainScreen,
    grid = undefined;

module.exports = {
    /**
     * initialise the view
     * @param  Object options Object of default overrised
     */
    init: function(options) {

        /**
         * defaults default setting for blessed
         * @type {Object}
         */
        var defaults = {
            smartCSR: true,
            fullUnicode: true,
            dockBorders: true,
            terminal: 'xterm-256color'
        };

        var opts = _.merge(options, defaults);

        mainScreen = blessed.screen(opts);

        mainScreen.on('resize', function() {
            mainScreen.render();
        });

        mainScreen.key(['escape', 'q', 'C-c'], function(ch, key) {
            return process.exit(0);
        });

        mainScreen.key(['r'], function(ch, key) {
            mainScreen.render();
        });
    },
    /**
     * title set the title of the blessed screen
     * @param  string title title to set
     * @return string the title just set
     */
    title: function(title) {
        mainScreen.title = title;
        return title;
    },
    /**
     * get the screen
     * @return Object the blessed screen
     */
    screen: function() {
        return mainScreen;
    },
    /**
     * provide a color to be used based on the status provided
     * @param  string status the status for which to provide the color
     * @return object a blessed color object
     */
    statusColor: function(status) {

        switch (status) {
            case "CREATE_IN_PROGRESS":
                return colors.yellow(status);
                break;
            case "CREATE_COMPLETE":
                return colors.green(status)
                break;
            case "CREATE_FAILED":
                return colors.red.bold(status)
                break;
            case "DELETE_COMPLETE":
                return colors.green(status)
                break;
            case "DELETE_FAILED":
                return colors.red.bold(status)
                break;
            case "DELETE_IN_PROGRESS":
                return colors.yellow(status)
                break;
            case "ROLLBACK_COMPLETE":
                return colors.green(status)
                break;
            case "ROLLBACK_FAILED":
                return colors.red.bold(status)
                break;
            case "ROLLBACK_IN_PROGRESS":
                return colors.yellow(status)
                break;
            case "UPDATE_COMPLETE":
                return colors.green(status)
                break;
            case "UPDATE_COMPLETE_CLEANUP_IN_PROGRESS":
                return colors.yellow(status)
                break;
            case "UPDATE_IN_PROGRESS":
                return colors.yellow(status)
                break;
            case "UPDATE_ROLLBACK_COMPLETE":
                return colors.green(status)
                break;
            case "UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS":
                return colors.green(status)
                break;
            case "UPDATE_ROLLBACK_FAILED":
                return colors.red.bold(status)
                break;
            case "UPDATE_ROLLBACK_IN_PROGRESS":
                return colors.yellow(status)
                break;
        }
    },
    /**
     * create / return a blessed grid object
     * @param  Object options override
     * @return {[type]} the grid obkec
     */
    grid: function(options) {

        if (!grid) {
            /**
             * defaults default setting for blessed
             * @type {Object}
             */
            var defaults = {
                rows: 12,
                cols: 12,
                fullUnicode: true,
                screen: mainScreen
            };

            var opts = _.merge(options, defaults),
                grid = new contrib.grid(opts);
        }

        return grid;
    }
}
